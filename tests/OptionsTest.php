<?php
declare(strict_types=1);

namespace HybrideLabs\OpeningHours\Tests;

use HybrideLabs\FluentOpeningHours\Options;
use PHPUnit\Framework\TestCase;

class OptionsTest extends TestCase
{

    /**
     * @test
     */
    public function returnsDefaultValues()
    {
        $options = new Options;

        $this->assertObjectHasAttribute('days', $options);
        $this->assertObjectHasAttribute('from', $options);
        $this->assertObjectHasAttribute('to', $options);
        $this->assertObjectHasAttribute('and', $options);
        $this->assertObjectHasAttribute('closed', $options);
    }

    /**
     * @test
     */
    public function returnsOverriddenValues()
    {
        $optionsArray = ['from' => 'de', 'to' => 'à'];
        $options      = new Options($optionsArray);

        $this->assertEquals("de", $options->from);
        $this->assertEquals("à", $options->to);

        $this->assertEquals("closed", $options->closed);
    }


    /**
     * @test
     */
    public function returnsStaticallyOverriddenValues()
    {
        $optionsArray = ['from' => 'de', 'to' => 'à'];
        $options      = Options::set($optionsArray);

        $this->assertEquals("de", $options->from);
        $this->assertEquals("à", $options->to);

        $this->assertEquals("closed", $options->closed);
    }
}
