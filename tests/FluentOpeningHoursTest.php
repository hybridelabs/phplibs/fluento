<?php
declare(strict_types=1);

namespace HybrideLabs\OpeningHours\Tests;

use Exception;
use HybrideLabs\FluentOpeningHours\Exceptions\ExcessiveDaysException;
use HybrideLabs\FluentOpeningHours\Exceptions\InsufficientDaysException;
use HybrideLabs\FluentOpeningHours\FluentOpeningHours;
use HybrideLabs\FluentOpeningHours\Options;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FluentOpeningHoursTest extends TestCase
{

    /**
     * @test
     */
    public function expectExceptionWhenEmpty()
    {
        $this->expectException(InvalidArgumentException::class);

        (new FluentOpeningHours())->parse();
    }

    /**
     * @test
     */
    public function expectTypeError()
    {
        $this->expectException(\TypeError::class);

        (new FluentOpeningHours())->parse('string');
    }


    /**
     * @return array
     */
    public function wrongArraysProvider()
    {
        return [
            'Missing days'  => [
                json_encode(
                    [
                        'hours'     => [0 => [], 1 => [], 2 => [], 3 => [], 4 => [], 5 => []],
                        'exception' => InsufficientDaysException::class,
                    ],
                ),
            ],
            'Too many days' => [
                json_encode(
                    [
                        'hours'     => [0 => [], 1 => [], 2 => [], 3 => [], 4 => [], 5 => [], 6 => [], 7 => []],
                        'exception' => ExcessiveDaysException::class,
                    ],
                ),
            ],
            'Wrong argument type' => [
                json_encode(
                    [
                        'hours' => [0 => 'string', 1 => [], 2 => [], 3 => [], 4 => [], 5 => [], 6 => []],
                        'exception' => InvalidArgumentException::class,
                    ]
                )
            ]
        ];
    }

    /**
     * @test
     * @dataProvider wrongArraysProvider
     */
    public function canOnlyAccessIfVariableInSession($data)
    {
        $data = json_decode($data);
        $this->expectException($data->exception);

        (new FluentOpeningHours())->parse($data->hours);

    }

    /**
     * @test
     */
    public function parsesCorrectlyEmptyValues()
    {
        $openingHoursArray = [0 => [], 1 => [], 2 => [], 3 => [], 4 => [], 5 => [], 6 => []];
        $expectedResult    = 'No opening data available';

        $openingHoursObject = new FluentOpeningHours(new Options());
        $this->assertEquals($expectedResult, $openingHoursObject->parse($openingHoursArray)->format());
    }

    /**
     * @test
     */
    public function formatsCorrectlyRandomOpeningsAndWithoutClosedDays()
    {
        $openingHoursArray  = [
            0 => ['10:30-12:00'], 1 => [], 2 => ['9:00-11:30'], 3 => ['8:00-10:00'], 4 => [], 5 => [], 6 => []
        ];
        $openingHoursObject = (new FluentOpeningHours)->parse($openingHoursArray)->openOnly();

        $expectedResult = 'monday from 10:30 to 12:00, wednesday from 9:00 to 11:30, thursday from 8:00 to 10:00';
        $this->assertTrue($openingHoursObject->format() == $expectedResult);
    }

    /**
     * @test
     */
    public function formatsCorrectlyWithFiveConsecutiveDays()
    {
        $openingHoursArray  = [
            0 => ['10:30-12:00'], 1 => ['10:30-12:00'], 2 => ['10:30-12:00'], 3 => ['10:30-12:00'], 4 => ['10:30-12:00'], 5 => [], 6 => []
        ];
        $openingHoursObject = (new FluentOpeningHours)->parse($openingHoursArray);

        $expectedResult = 'monday, tuesday, wednesday, thursday and friday from 10:30 to 12:00, saturday and sunday closed';
        $this->assertTrue($openingHoursObject->format() == $expectedResult);
    }

    /**
     * @test
     */
    public function formatsCorrectlyComplexSetupWithIntermittentDays()
    {
        $openingHoursArray  = [
            0 => ['10:30-12:00'], 1 => ['10:30-12:00'], 2 => [], 3 => ['10:30-12:00'], 4 => ['10:30-12:00'], 5 => ['9:00-12:00', '14:00-18:00'], 6 => []
        ];
        $openingHoursObject = (new FluentOpeningHours)->parse($openingHoursArray);

        $expectedResult = 'monday, tuesday, thursday and friday from 10:30 to 12:00, saturday from 9:00 to 12:00 and from 14:00 to 18:00, wednesday and sunday closed';
        $this->assertTrue($openingHoursObject->format() == $expectedResult);
    }

    /**
     * @test
     */
    public function formatsCorrectlyWithOverriddenOptions()
    {
        $openingHoursArray  = [
            0 => ['10:30-12:00'], 1 => ['10:30-12:00'], 2 => [], 3 => [], 4 => [], 5 => ['9:00-12:00', '14:00-18:00'], 6 => []
        ];
        $options = [
            'from' => 'de',
            'to' => 'à',
            'and' => 'et',
            'days' => ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'],
        ];
        $openingHoursObject = (new FluentOpeningHours(Options::set($options)))->parse($openingHoursArray)->openOnly();

        $expectedResult = 'lundi et mardi de 10:30 à 12:00, samedi de 9:00 à 12:00 et de 14:00 à 18:00';
        $this->assertTrue($openingHoursObject->format() == $expectedResult);
    }

    /**
     * @test
     */
    public function formatsCorrectlyWhileCalledStatically()
    {
        $openingHoursArray  = [
            0 => ['10:30-12:00'], 1 => ['10:30-12:00'], 2 => [], 3 => ['10:30-12:00'], 4 => ['10:30-12:00'], 5 => ['9:00-12:00', '14:00-18:00'], 6 => []
        ];
        $openingHoursObject = FluentOpeningHours::parse($openingHoursArray);

        $expectedResult = 'monday, tuesday, thursday and friday from 10:30 to 12:00, saturday from 9:00 to 12:00 and from 14:00 to 18:00, wednesday and sunday closed';
        $this->assertTrue($openingHoursObject->format() == $expectedResult);
    }

    /**
     * @test
     */
    public function formatsCorrectlyWhileCalledStaticallyWithOverriddenOptions()
    {
        $openingHoursArray  = [
            0 => ['10:30-12:00'], 1 => ['10:30-12:00'], 2 => [], 3 => [], 4 => [], 5 => ['9:00-12:00', '14:00-18:00'], 6 => []
        ];
        $options = [
            'from' => 'de',
            'to' => 'à',
            'and' => 'et',
            'days' => ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'],
        ];
        $openingHoursObject = FluentOpeningHours::parse($openingHoursArray, Options::set($options))->openOnly();

        $expectedResult = 'lundi et mardi de 10:30 à 12:00, samedi de 9:00 à 12:00 et de 14:00 à 18:00';
        $this->assertTrue($openingHoursObject->format() == $expectedResult);
    }
}
