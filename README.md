# Opening Hours

Formatting opening hours in a fluent written way:

```
$openingHours = [
    0 => ['10:30-12:00'], 
    1 => [], 
    2 => ['9:00-11:30'], 
    3 => ['8:00-10:00', '14:00-18:00'], 
    4 => [], 
    5 => [], 
    6 => [],
];
$fluentOpeningHours = (new FluentOpeningHours)->parse($openingHours);

echo $fluentOpeningHours->format();
```

results in :

```
monday from 10:30 to 12:00, wednesday from 9:00 to 11:30, thursday from 8:00 to 10:00 and from 14:00 to 18:00, tuesday, friday, saturday and sunday closed
```

## Methods

You can omit the closed days by calling ```$fluentOpeningHours->openOnly()``` before formatting the output.


## Options

All strings can be replaced by providing the Option object with new values and feeding it to the FluentOpeningHours class:

```
$options = [
    'from' => 'de',
    'to' => 'à',
    'and' => 'et',
    'closed' => 'fermé',
    'days' => ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'],
];
```

which changes the result to:

```
lundi de 10:30 à 12:00, mercredi de 9:00 à 11:30, jeudi de 8:00 à 10:00 et de 14:00 à 18:00, mardi, vendredi, samedi et dimanche fermé
```


